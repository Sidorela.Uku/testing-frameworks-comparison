import { test, expect } from '@playwright/test';

test('cy.location() - get window.location', async ({ page }) => {
    await page.goto('http://localhost:8080/commands/location');
    const location = await page.evaluate(() => ({
        hash: window.location.hash,
        href: window.location.href,
        host: window.location.host,
        hostname: window.location.hostname,
        origin: window.location.origin,
        pathname: window.location.pathname,
        port: window.location.port,
        protocol: window.location.protocol,
        search: window.location.search,
    }));

    expect(location.hash).toBe('');
    expect(location.href).toBe('http://localhost:8080/commands/location');
    expect(location.host).toBe('localhost:8080');
    expect(location.hostname).toBe('localhost');
    expect(location.origin).toBe('http://localhost:8080');
    expect(location.pathname).toBe('/commands/location');
    expect(location.port).toBe('8080');
    expect(location.protocol).toBe('http:');
    expect(location.search).toBe('');
});

test('cy.url() - get the current URL', async ({ page }) => {
    await page.goto('http://localhost:8080/commands/location');
    
    const url = await page.url();
    expect(url).toBe('http://localhost:8080/commands/location');
});

