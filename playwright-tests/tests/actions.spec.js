const { test, expect } = require('@playwright/test');

test('.type() - type into a DOM element', async ({ page }) => {
    await page.goto('http://localhost:8080/commands/actions');
 
    await page.fill('.action-email', 'fake@email.com');
    const emailValue = await page.$eval('.action-email', el => el.value);
    expect(emailValue).toBe('fake@email.com');
  
    // .type() with special character sequences
    await page.keyboard.press('ArrowLeft');
    await page.keyboard.press('ArrowRight');
    await page.keyboard.press('ArrowUp');
    await page.keyboard.press('ArrowDown');
    await page.keyboard.press('Delete');
    await page.keyboard.down('Control');
    await page.keyboard.press('A');
    await page.keyboard.up('Control');
    await page.keyboard.press('Backspace');
  
    // .type() with key modifiers
    await page.keyboard.down('Alt');
    await page.keyboard.up('Alt');
    await page.keyboard.down('Control');
    await page.keyboard.up('Control');
    await page.keyboard.down('Meta');
    await page.keyboard.up('Meta');
    await page.keyboard.down('Shift');
    await page.keyboard.up('Shift');
  
    // Delay each keypress by 0.1 sec
    await page.type('.action-email', 'slow.typing@email.com', { delay: 100 });
    const slowTypingValue = await page.$eval('.action-email', el => el.value);
    expect(slowTypingValue).toBe('slow.typing@email.com');
  
    // Playwright does not have an equivalent for { force: true } option
    // You'll need to ensure the element is interactable before typing.
  });
  
test('.focus() - focus on a DOM element', async ({ page }) => {
  await page.goto('http://localhost:8080/commands/actions');

  await page.focus('.action-focus');
  const hasFocusClass = await page.$eval('.action-focus', el => el.classList.contains('focus'));
  expect(hasFocusClass).toBeTruthy();

  const previousSiblingStyle = await page.$eval('.action-focus', el => el.previousElementSibling.style.color);
  expect(previousSiblingStyle).toBe('orange');
});

test('.blur() - blur off a DOM element', async ({ page }) => {
  await page.goto('http://localhost:8080/commands/actions');

  await page.fill('.action-blur', 'About to blur');
  await page.$eval('.action-blur', el => el.blur());

  const hasErrorClass = await page.$eval('.action-blur', el => el.classList.contains('error'));
  expect(hasErrorClass).toBeTruthy();

  const previousSiblingStyle = await page.$eval('.action-blur', el => el.previousElementSibling.style.color);
  expect(previousSiblingStyle).toBe('red');
});

test('.clear() - clears an input or textarea element', async ({ page }) => {
  await page.goto('http://localhost:8080/commands/actions');

  await page.fill('.action-clear', 'Clear this text');
  const inputValueBeforeClear = await page.$eval('.action-clear', el => el.value);
  expect(inputValueBeforeClear).toBe('Clear this text');

  await page.fill('.action-clear', ''); // Clear the input
  const inputValueAfterClear = await page.$eval('.action-clear', el => el.value);
  expect(inputValueAfterClear).toBe('');
});
  
test('.submit() - submit a form', async ({ page }) => {
  await page.goto('http://localhost:8080/commands/actions');

  await page.fill('.action-form [type="text"]', 'HALFOFF');
  await page.press('.action-form [type="text"]', 'Enter'); // Assuming pressing Enter submits the form

  // Wait for the form submission to complete and the next element to contain the expected text
  await page.waitForSelector('.action-form + *:text("Your form has been submitted!")');
});
  
test('.dblclick() - double click on a DOM element', async ({ page }) => {
  // Assuming your app has a listener on 'dblclick' event in 'scripts.js'
  // that hides the div and shows an input on double click
  await page.goto('http://localhost:8080/commands/actions');

  await page.dblclick('.action-div');
  
  // Wait for the changes to take effect
  await page.waitForSelector('.action-div', { state: 'hidden' });
  await page.waitForSelector('.action-input-hidden', { state: 'visible' });
});


test('.select() - select an option in a <select> element', async ({ page }) => {
  await page.goto('http://localhost:8080/commands/actions');

  // at first, no option should be selected
  const initialSelectedValue = await page.$eval('.action-select', el => el.value);
  expect(initialSelectedValue).toBe('--Select a fruit--');

  // Select option(s) with matching text content
  await page.selectOption('.action-select', 'apples');
  const selectedValueAfterSingleSelect = await page.$eval('.action-select', el => el.value);
  expect(selectedValueAfterSingleSelect).toBe('fr-apples');

  // const selectedValuesAfterMultipleSelect = await page.$$eval('.action-select-multiple option[selected]', options => options.map(option => option.value));
  // expect(selectedValuesAfterMultipleSelect).toEqual(['fr-apples', 'fr-oranges', 'fr-bananas']);
  await page.selectOption('.action-select-multiple', ['apples', 'oranges', 'bananas']);
  const hasOrangesOption = await page.$eval('.action-select-multiple', (select) => {
    const options = Array.from(select.options);
    return options.some(option => option.value === 'fr-oranges');
  });
  
  expect(hasOrangesOption).toBeTruthy();

  const hasApplesOption = await page.$eval('.action-select-multiple', (select) => {
    const options = Array.from(select.options);
    return options.some(option => option.value === 'fr-apples');
  });
  
  expect(hasApplesOption).toBeTruthy();

  const hasBananasOption = await page.$eval('.action-select-multiple', (select) => {
    const options = Array.from(select.options);
    return options.some(option => option.value === 'fr-bananas');
  });
  
  expect(hasBananasOption).toBeTruthy();

  // Select option(s) with matching value
  await page.selectOption('.action-select', 'fr-bananas');
  const selectedValueAfterValueSelect = await page.$eval('.action-select', el => el.value);
  expect(selectedValueAfterValueSelect).toBe('fr-bananas');

  await page.selectOption('.action-select-multiple', ['fr-apples', 'fr-oranges', 'fr-bananas']);
  expect(hasOrangesOption).toBeTruthy();
  expect(hasApplesOption).toBeTruthy();
  expect(hasBananasOption).toBeTruthy();

  // assert the selected values include oranges
  const selectedValues = await page.$$eval('.action-select-multiple option[selected]', options => options.length);
  // expect(selectedValues).toBe(3);
});

// test('.scrollIntoView() - scroll an element into view', async t => {
//   const scrollHorizontalButton = Selector('#scroll-horizontal button');
//   const scrollVerticalButton = Selector('#scroll-vertical button');
//   const scrollBothButton = Selector('#scroll-both button');

//   // Verify that initially the buttons are not visible
//   // await t.expect(scrollVerticalButton.exists).notOk();
//   // await t.expect(scrollBothButton.exists).notOk();

//   // Scroll the parent elements of the buttons into view
//   await t.scrollIntoView(scrollVerticalButton.parent());
//   await t.scrollIntoView(scrollBothButton.parent());

//   // Verify that the buttons are now visible
//   await t.expect(scrollVerticalButton.exists).ok();
//   await t.expect(scrollBothButton.exists).ok();
// });

test('.trigger() - trigger an event on a DOM element', async ({ page }) => {
  await page.goto('http://localhost:8080/commands/actions');

  // To interact with a range input (slider)
  // we need to set its value & trigger the
  // event to signal it changed

  // Here, we set the value directly and trigger the 'change' event
  await page.$eval('.trigger-input-range', (inputRange) => {
    inputRange.value = 25;
    inputRange.dispatchEvent(new Event('input', { bubbles: true }));
    inputRange.dispatchEvent(new Event('change', { bubbles: true }));
  });

  // Assertion after triggering the event
  const rangeValueText = await page.$eval('.trigger-input-range + p', (p) => p.textContent.trim());
  expect(rangeValueText).toBe('25');
});
