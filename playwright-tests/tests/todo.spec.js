// @ts-check
const { test, expect } = require('@playwright/test');

const TODO_ITEMS = [
  'Pay electric bill',
  'Walk the dog'
];

  
test('has title', async ({ page }) => {
  await page.goto('http://localhost:8080/todo');

  // Expect a title "to contain" a substring.
  await expect(page.getByTestId('todo-list-id')).toHaveText([
    TODO_ITEMS[0],
    TODO_ITEMS[1]
  ]);
});

test('can add new todo items', async ({ page }) => {
  await page.goto('http://localhost:8080/todo');
  // We'll store our item text in a variable so we can reuse it
  const newItem = 'Feed the cat';

  await page.fill('[data-test=new-todo]', newItem);
  await page.press('[data-test=new-todo]', 'Enter');

  const todoListItems = await page.$$('.todo-list li');
  expect(todoListItems.length).toBe(3);

  const lastItem = todoListItems[todoListItems.length - 1];
  const lastItemText = await lastItem.textContent();
  expect(lastItemText).toBe(newItem);
});

test('can check off an item as completed', async ({ page }) => {
  await page.goto('http://localhost:8080/todo');
  // Assuming 'Pay electric bill' is the text of the todo item
  const todoItemText = 'Pay electric bill';

  // Find the checkbox associated with the todo item text
  const checkbox = await page.$(`//label[contains(text(),'${todoItemText}')]/preceding-sibling::input[@type='checkbox']`);
  await checkbox.check();

  // Wait for the item to be marked as completed
  await page.waitForSelector(`//label[contains(text(),'${todoItemText}')]/ancestor::li[contains(@class,'completed')]`);
});

test('can filter for uncompleted tasks', async ({ page }) => {
  await page.goto('http://localhost:8080/todo');
  // Assuming 'Pay electric bill' is the text of the todo item
  const todoItemText = 'Pay electric bill';

    // Find and check the checkbox associated with the todo item text
  const checkbox = await page.$(`//label[contains(text(),'${todoItemText}')]/preceding-sibling::input[@type='checkbox']`);
  await checkbox.check();
  // Click on the "active" button to display only incomplete items
  await page.click('text=Active');

  // Assert that there is only one incomplete item in the list
  const incompleteTodoItems = await page.$$('.todo-list li');
  expect(incompleteTodoItems.length).toBe(1);

  // Assert that the first item is 'Walk the dog'
  const firstIncompleteItemText = await incompleteTodoItems[0].textContent();
  expect(firstIncompleteItemText.trim()).toBe('Walk the dog');

  // Assert that the task we checked off does not exist on the page
  const isTodoItemPresent = await page.isVisible(`text=${todoItemText}`);
  expect(isTodoItemPresent).toBe(false);
});

test('can filter for completed tasks', async ({ page }) => {
  await page.goto('http://localhost:8080/todo');
  // Assuming 'Pay electric bill' is the text of the todo item
  const todoItemText = 'Pay electric bill';

    // Find and check the checkbox associated with the todo item text
  const checkbox = await page.$(`//label[contains(text(),'${todoItemText}')]/preceding-sibling::input[@type='checkbox']`);
  await checkbox.check();
  // Click on the "Completed" button to display only completed items
  await page.click('text=Completed');

  // Assert that there is only one completed item in the list
  const completedTodoItems = await page.$$('.todo-list li');
  expect(completedTodoItems.length).toBe(1);

  // Assert that the first item is 'Pay electric bill'
  const firstCompletedItemText = await completedTodoItems[0].textContent();
  expect(firstCompletedItemText.trim()).toBe('Pay electric bill');

  // Assert that 'Walk the dog' does not exist on the page
  const isWalkTheDogPresent = await page.isVisible('text=Walk the dog');
  expect(isWalkTheDogPresent).toBe(false);
});

test('can delete all completed tasks', async ({ page }) => {
  await page.goto('http://localhost:8080/todo');
  // Assuming 'Pay electric bill' is the text of the todo item
  const todoItemText = 'Pay electric bill';

    // Find and check the checkbox associated with the todo item text
  const checkbox = await page.$(`//label[contains(text(),'${todoItemText}')]/preceding-sibling::input[@type='checkbox']`);
  await checkbox.check();
  // First, let's click the "Clear completed" button
  await page.click('text=Clear completed');

  // Then we can make sure that there is only one element in the list and our element does not exist
  const todoListItems = await page.$$('.todo-list li');
  expect(todoListItems.length).toBe(1);

  // Assert that 'Pay electric bill' does not exist on the page
  const isPayElectricBillPresent = await page.isVisible('text=Pay electric bill');
  expect(isPayElectricBillPresent).toBe(false);

  // Finally, make sure that the clear button no longer exists.
  const isClearCompletedButtonPresent = await page.isVisible('text=Clear completed');
  expect(isClearCompletedButtonPresent).toBe(false);
});

