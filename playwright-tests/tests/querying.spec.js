import { test, expect } from '@playwright/test';

test('cy.get() - query DOM elements', async ({ page }) => {
  await page.goto('http://localhost:8080/commands/querying');

  await expect(page.locator('#query-btn')).toHaveText('Button');
  await expect(page.locator('.query-btn')).toHaveText('Button');
  
  // We'll use evalateHandle to execute a script in the browser context
  const buttonInWell = await page.evaluateHandle(() => {
    return document.querySelector('#querying .well>button');
  });
  const buttonText = await buttonInWell.innerText();
  expect(buttonText).toBe('Button');

  const exampleElement = await page.locator('[data-test-id="test-example"]');
  await expect(exampleElement).toHaveClass('example');

  const dataTestId = await exampleElement.getAttribute('data-test-id');
  expect(dataTestId).toBe('test-example');

  const positionCss = await exampleElement.evaluate((el) => getComputedStyle(el).position);
  expect(positionCss).toBe('static');
});

test('.within() - query DOM elements within a specific element', async ({ page }) => {
    await page.goto('http://localhost:8080/commands/querying');
    
    const queryForm = await page.locator('.query-form');

    // Query elements within the parent element
    const firstInput = await queryForm.locator('input:first-child');
    await expect(firstInput).toHaveAttribute('placeholder', 'Email');
  
    const lastInput = await queryForm.locator('input:last-child');
    await expect(lastInput).toHaveAttribute('placeholder', 'Password');
  });

test('cy.root() - query the root DOM element', async ({ page }) => {
// Navigate to the page
  await page.goto('http://localhost:8080/commands/querying');

  // Check if the root element is the document
  const rootElement = await page.locator('html').first();
  expect(rootElement).not.toBeNull();

  // Query elements within .query-ul
  const queryUl = await page.locator('.query-ul');
//   const queryUlElements = await queryUl.locateAll('li');

  // Check if the root element is now the ul DOM element
//   expect(queryUlElements.length).toBeGreaterThan(0);
});
