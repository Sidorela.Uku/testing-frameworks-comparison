const { exec } = require('child_process');

const command = 'npx playwright test tests --project chromium';

const startTime = new Date();

exec(command, (error) => {
  if (error) {
    console.error(`Error executing TestCafe command: ${error}`);
    return;
  }
    
  const endTime = new Date();
  const executionTime = endTime - startTime;

  console.log(`Test execution started at: ${startTime}`);
  console.log(`Test execution ended at: ${endTime}`);
  console.log(`Total execution time: ${executionTime} milliseconds`);
});
