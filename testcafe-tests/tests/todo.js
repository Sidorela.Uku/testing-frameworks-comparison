import { Selector } from 'testcafe';

fixture('Todo App Test')
    .page('http://localhost:8080/todo');

// const payElectricBillSelector = Selector('.todo-list li').withText('Pay electric bill');
// const walkTheDogSelector = Selector('.todo-list li').withText('Walk the dog');

test('Displays two todo items by default', async t => {
    await t
        .expect(Selector('.todo-list li').count).eql(2)
        .expect(Selector('.todo-list li').nth(0).innerText).eql('Pay electric bill')
        .expect(Selector('.todo-list li').nth(-1).innerText).eql('Walk the dog');
});

test('can add new todo items', async t => {
    const newItem = 'Feed the cat';
  
    await t
      .typeText('[data-test=new-todo]', newItem)
      .pressKey('enter')
      .expect(Selector('.todo-list li').count).eql(3)
      .expect(Selector('.todo-list li').nth(-1).innerText).eql(newItem);
  });  

test('can check off an item as completed', async t => {
    await t
      .expect(Selector('li').withText('Pay electric bill').find('input[type=checkbox]').checked).notOk()
      .click(Selector('li').withText('Pay electric bill').find('input[type=checkbox]'))
      .expect(Selector('li').withText('Pay electric bill').hasClass('completed')).ok();
  });

test('can filter for uncompleted tasks', async t => {
  // Click on the "Pay electric bill" task to check it off
  await t.click(Selector('.todo-list li').withText('Pay electric bill').find('input[type=checkbox]'));

  // Click on the "Active" button to filter for uncompleted tasks
  await t.click(Selector('a').withText('Active'));

  // Assert that there is only one incomplete item in the list
  await t
    .expect(Selector('.todo-list li').count).eql(1)
    .expect(Selector('.todo-list li').withText('Walk the dog').exists).ok();

  // Assert that the checked off task does not exist on the page
  await t.expect(Selector('.todo-list li').withText('Pay electric bill').exists).notOk();
});

test('can filter for completed tasks', async t => {
    // Click on the "Pay electric bill" task to check it off
    await t.click(Selector('.todo-list li').withText('Pay electric bill').find('input[type=checkbox]'));

    // Click on the "Completed" button to filter for completed tasks
    await t.click(Selector('a').withText('Completed'));
  
    // Assert that there is only one completed item in the list
    await t
      .expect(Selector('.todo-list li').count).eql(1)
      .expect(Selector('.todo-list li').withText('Pay electric bill').exists).ok();
  
    // Assert that the incomplete task does not exist on the page
    await t.expect(Selector('.todo-list li').withText('Walk the dog').exists).notOk();
  });

test('can delete all completed tasks', async t => {
  // Click on the "Pay electric bill" task to check it off
  await t.click(Selector('.todo-list li').withText('Pay electric bill').find('input[type=checkbox]'));

  // Click the "Clear completed" button
  await t.click(Selector('button').withText('Clear completed'));

  // Verify that only one element remains in the list and it's not the completed task
  const todoList = Selector('.todo-list li');
  await t.expect(todoList.count).eql(1);
  await t.expect(todoList.textContent).notContains('Pay electric bill');

  // Verify that the "Clear completed" button no longer exists
  await t.expect(Selector('button').withText('Clear completed').exists).notOk();
});
