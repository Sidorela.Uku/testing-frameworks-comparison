import { Selector } from 'testcafe';

fixture`Querying`.page`http://localhost:8080/commands/querying`;

test('cy.get() - query DOM elements', async t => {
  // Assert that an element with id 'query-btn' contains 'Button'
  await t.expect(Selector('#query-btn').innerText).contains('Button');

  // Assert that an element with class 'query-btn' contains 'Button'
  await t.expect(Selector('.query-btn').innerText).contains('Button');

  // Assert that the first button inside an element with id 'querying' and class 'well' contains 'Button'
  await t.expect(Selector('#querying .well>button').nth(0).innerText).contains('Button');

  // Assert that an element with data-test-id 'test-example' has class 'example'
  await t.expect(Selector('[data-test-id="test-example"]').hasClass('example')).ok();

  // Assert that an element with data-test-id 'test-example' has attribute 'data-test-id' equal to 'test-example'
  await t.expect(Selector('[data-test-id="test-example"]').getAttribute('data-test-id')).eql('test-example');

  // Assert that an element with data-test-id 'test-example' has CSS property 'position' equal to 'static'
  await t.expect(Selector('[data-test-id="test-example"]').getStyleProperty('position')).eql('static');
});

test('.within() - query DOM elements within a specific element', async t => {
    // Select the element with class 'query-form' and then perform queries within it
    const queryForm = Selector('.query-form');

    // Perform queries within the '.query-form' element
    const input1 = queryForm.find('input').nth(0);
    const input2 = queryForm.find('input').nth(-1);
  
    // Assert that the first input inside the '.query-form' element has placeholder 'Email'
    await t.expect(input1.getAttribute('placeholder')).eql('Email');
  
    // Assert that the last input inside the '.query-form' element has placeholder 'Password'
    await t.expect(input2.getAttribute('placeholder')).eql('Password');
  });

  test('cy.root() - query the root DOM element', async t => {
    // Assert that the root DOM element is 'html'
    await t.expect(Selector('html').exists).ok();
  
    // Query within the '.query-ul' element
    await t.expect(Selector('.query-ul').exists).ok();
    await t.expect(Selector('.query-ul').hasClass('query-ul')).ok();
  });
