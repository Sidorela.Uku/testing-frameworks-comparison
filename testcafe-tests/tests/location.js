import { Selector } from 'testcafe';

fixture`Location`.page`http://localhost:8080/commands/location`;


test('cy.location() - get window.location', async t => {
  const location = await t.eval(() => window.location);

  await t.expect(location.hash).eql('');
  await t.expect(location.href).eql('http://localhost:8080/commands/location');
  await t.expect(location.host).eql('localhost:8080');
  await t.expect(location.hostname).eql('localhost');
  await t.expect(location.origin).eql('http://localhost:8080');
  await t.expect(location.pathname).eql('/commands/location');
  await t.expect(location.port).eql('8080');
  await t.expect(location.protocol).eql('http:');
  await t.expect(location.search).eql('');
});

test('cy.url() - get the current URL', async t => {
  await t.expect(await t.eval(() => window.location.href)).eql('http://localhost:8080/commands/location');
});
