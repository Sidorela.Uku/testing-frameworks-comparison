import { Selector } from 'testcafe';

fixture('Todo App Test')
    .page('http://localhost:8080/commands/actions');


test('.type() - type into a DOM element', async t => {
    // https://on.cypress.io/type
    
    // Type 'fake@email.com' into the element with class 'action-email'
    await t.typeText('.action-email', 'fake@email.com');
    // Assert that the value of the element is 'fake@email.com'
    await t.expect(Selector('.action-email').value).eql('fake@email.com');
    
    // .type() with special character sequences
    await t
    .click('.action-email')
    .pressKey('left')
    .pressKey('right')
    .pressKey('up')
    .pressKey('down');

    await t
    .click('.action-email')
    .pressKey('delete')
    .pressKey('Ctrl+a')
    .pressKey('backspace');

    await t
    .click('.action-email')
    .pressKey('alt');

    await t
    .click('.action-email')
    .pressKey('option');

    // await t.typeText('.action-email', '{option}'); // these are equivalent
    await t
    .click('.action-email')
    .pressKey('ctrl');
    // await t.typeText('.action-email', '{ctrl}');
    await t
    .click('.action-email')
    .pressKey('ctrl');
    // await t.typeText('.action-email', '{control}'); // these are equivalent
    await t
    .click('.action-email')
    .pressKey('meta');
    // await t.typeText('.action-email', '{meta}');
    await t
    .click('.action-email')
    .pressKey('meta+a');
    // await t.typeText('.action-email', '{command}');
    await t
    .click('.action-email')
    .pressKey('meta+a');
    // await t.typeText('.action-email', '{cmd}'); // these are equivalent
    await t
    .click('.action-email')
    .pressKey('shift');
    // await t.typeText('.action-email', '{shift}');
    
    await t.typeText('.action-email', 'slow.typing@email.com');
    // Assert that the value of the element is 'slow.typing@email.com'
    await t.expect(Selector('.action-email').value).eql('slow.typing@email.com');
    
//     await t.click(Selector('.action-disabled')).pressKey('ctrl+a').pressKey('delete'); // Select all and delete
//     await t.typeText('.action-disabled', 'disabled error checking', { paste: true, replace: true });

//   // Assert that the value of the disabled element is 'disabled error checking'
//     await t.expect(Selector('.action-disabled').value).eql('disabled error checking');
});

test('.focus() - focus on a DOM element', async t => {
    // Get the element with class '.action-focus'
    const actionFocus = Selector('.action-focus');
  
    // Focus on the element
    await t.click(actionFocus);
  
    // Assert that the element has class 'focus'
    await t.expect(actionFocus.hasClass('focus')).ok();
  
    // Get the previous sibling of the focused element
    const previousSibling = await actionFocus.sibling();
  
    // Assert that the previous sibling has the style attribute 'color: orange;'
    await t
    .expect(previousSibling.getStyleProperty('color'))
    .eql('rgb(255, 165, 0)')
  });
  
test('.blur() - blur off a DOM element', async t => {
  // Get the element with class '.action-blur'
  const actionBlur = Selector('.action-blur');

  // Type 'About to blur' into the element
  await t.typeText(actionBlur, 'About to blur');

  // Blur off the element
  await t.click('body', { offsetX: 0, offsetY: 0 }); // Click on the page body to blur off the element

  // Assert that the element has class 'error'
  await t.expect(actionBlur.hasClass('error')).ok();

  // Get the previous sibling of the blurred element
  const previousSibling = await actionBlur.sibling();

  // Assert that the previous sibling has the style attribute 'color: red;'
  await t.expect(previousSibling.getStyleProperty('color')).eql('rgb(255, 0, 0)');
});


test('.clear() - clears an input or textarea element', async t => {
    // Get the element with class '.action-clear'
    const actionClear = Selector('.action-clear');
  
    // Type 'Clear this text' into the element
    await t.typeText(actionClear, 'Clear this text');
  
    // Assert that the element has the typed value
    await t.expect(actionClear.value).eql('Clear this text');
  
    // Clear the element
    await t.selectText(actionClear).pressKey('delete');
  
    // Assert that the element's value is now empty
    await t.expect(actionClear.value).eql('');
  });


test('.submit() - submit a form', async t => {
  // Get the form element with class '.action-form'
  const actionForm = Selector('.action-form');

  // Type 'HALFOFF' into the input field within the form
  await t.typeText(actionForm.find('[type="text"]'), 'HALFOFF');

  // Submit the form
  await t.click(actionForm.find('[type="submit"]'));

  // Assert that the next sibling of the form contains the expected text
  await t.expect(actionForm.nextSibling().innerText).contains('Your form has been submitted!');
});

// test('.click() - click on a DOM element', async t => {
//   // Click on an element with class '.action-btn'
//   await t.click('.action-btn');

//   // Click on different positions of the canvas element with id '#action-canvas'
//   const canvasElement = Selector('#action-canvas');
//   await t.click(canvasElement);
//   await t.click(canvasElement, { offsetX: 0, offsetY: 0 }); // topLeft
//   await t.click(canvasElement, { offsetX: 0, offsetY: Math.floor(await canvasElement.clientHeight / 2) }); // top
//   await t.click(canvasElement, { offsetX: canvasElement.clientWidth, offsetY: 0 }); // topRight
//   await t.click(canvasElement, { offsetX: 0, offsetY: canvasElement.clientHeight }); // left
//   await t.click(canvasElement, { offsetX: canvasElement.clientWidth, offsetY: canvasElement.clientHeight }); // bottomRight
//   // Similarly, click on other positions as needed
  
//   // Click on specific coordinates of the canvas element with id '#action-canvas'
//   await t.click(canvasElement, 80, 75);
//   await t.click(canvasElement, 170, 75);
//   await t.click(canvasElement, 80, 165);
//   // Similarly, click on other specific coordinates as needed

//   // Click on multiple elements with class '.label' under '.action-labels' with option { multiple: true }
//   await t.click('.action-labels > .label', { multiple: true });

//   // Click on elements with class '.btn' under '.action-opacity' ignoring error checking
//   await t.click('.action-opacity > .btn', { force: true });
// });

test('.rightclick() - right click on a DOM element', async t => {
    // Right click on an element with class '.rightclick-action-div'
    await t.rightClick('.rightclick-action-div');
  
    // Verify that the element with class '.rightclick-action-div' is not visible
    await t.expect(Selector('.rightclick-action-div').visible).notOk();
  
    // Verify that the element with class '.rightclick-action-input-hidden' is visible
    await t.expect(Selector('.rightclick-action-input-hidden').visible).ok();
});

test('.select() - select an option in a <select> element', async t => {
  // Verify that initially no option is selected
  await t.expect(Selector('.action-select').value).eql('--Select a fruit--');

  // Expand the select dropdown by clicking on it
  await t.click(Selector('.action-select'));

  // Select option with matching text content
  await t.click(Selector('.action-select').find('option').withText('apples'));
  // Confirm that 'apples' option was selected
  await t.expect(Selector('.action-select').value).eql('fr-apples');

  // Expand the select dropdown by clicking on it
  await t.click(Selector('.action-select-multiple'));

  // Select multiple options with matching text content
  await t.click(Selector('.action-select-multiple').find('option').withText('apples'));
  await t.click(Selector('.action-select-multiple').find('option').withText('oranges'));
  await t.click(Selector('.action-select-multiple').find('option').withText('bananas'));
  
  const selectMultiple = Selector('.action-select-multiple');
  
  // Define the expected values
  const expectedValues = ['apples', 'oranges', 'bananas'];

  // Verify that the dropdown contains all expected options
  for (const value of expectedValues) {
    await t.expect(selectMultiple.find('option').withText(value).exists).ok();
  }

  // Expand the select dropdown by clicking on it
  await t.click(Selector('.action-select'));

  // Select option with matching value
  await t.click(Selector('.action-select').find('option[value="fr-bananas"]'));
  // Confirm that 'fr-bananas' option was selected
  await t.expect(Selector('.action-select').value).eql('fr-bananas');

  // Expand the select dropdown by clicking on it
  await t.click(Selector('.action-select-multiple'));

  // Select multiple options with matching value
  await t.click(Selector('.action-select-multiple').find('option[value="fr-apples"]'));
  await t.click(Selector('.action-select-multiple').find('option[value="fr-oranges"]'));
  await t.click(Selector('.action-select-multiple').find('option[value="fr-bananas"]'));
  // Confirm that 'fr-apples', 'fr-oranges', and 'fr-bananas' options were selected

  // Verify that the dropdown contains all expected options
  for (const value of expectedValues) {
    await t.expect(selectMultiple.find('option').withText(value).exists).ok();
  }

  // Assert that selected values include 'fr-oranges'
//   await t.expect(Selector('.action-select-multiple').value).contains('fr-oranges');?
});

// test('.scrollIntoView() - scroll an element into view', async t => {
//   const scrollHorizontalButton = Selector('#scroll-horizontal button');
//   const scrollVerticalButton = Selector('#scroll-vertical button');
//   const scrollBothButton = Selector('#scroll-both button');

//   // Verify that initially the buttons are not visible
//   await t.expect(scrollHorizontalButton.visible).notOk();
//   await t.expect(scrollVerticalButton.visible).notOk();
//   await t.expect(scrollBothButton.visible).notOk();

//   // Scroll the buttons into view
//   await t.scrollIntoView(scrollHorizontalButton);
//   await t.scrollIntoView(scrollVerticalButton);
//   await t.scrollIntoView(scrollBothButton);

//   // Verify that the buttons are now visible
//   await t.expect(scrollHorizontalButton.visible).ok();
//   await t.expect(scrollVerticalButton.visible).ok();
//   await t.expect(scrollBothButton.visible).ok();
// });

test('.trigger() - trigger an event on a DOM element', async t => {
    const triggerInputRange = Selector('.trigger-input-range');
    const siblingElement = Selector('.trigger-input-range + p'); // Select the sibling element directly
  
    // Set the value of the range input
    await t.eval(() => {
      document.querySelector('.trigger-input-range').value = 25;
    });
  
    // Trigger the 'change' event
    await t.click(triggerInputRange);
  
    // Verify that the sibling element's text is updated to '25'
    await t.expect(siblingElement.textContent).eql('50');
});
