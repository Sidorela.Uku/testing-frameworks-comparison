# Testing frameworks comparison

This repository is a documentation for the code of a research paper work for ICTE masters subject. 

Cypress-tests folder includes code forked from the original repository from Cypress in GitHub. (https://github.com/cypress-io/cypress-example-kitchensink)

playwright-tests folder includes the same codes written for playwright framework. 

testcafe-tests folder includes the same codes written for testcafe framework. 


